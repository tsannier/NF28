package Controller;

import Model.*;
import javafx.collections.ListChangeListener;
import javafx.collections.MapChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class ContactController implements Initializable {

    private static Image _groupIcon, _contactIcon;

    @FXML
    MenuItem openFile, saveFile, saveAs;

    @FXML
    private  TreeView<Object> tree;

    @FXML
    private VBox containerContact;

    @FXML
    private TextField firstname;

    @FXML
    private TextField lastname;

    @FXML
    private TextField street;

    @FXML
    private TextField postcode;

    @FXML
    private TextField city;

    @FXML
    private ComboBox<Country> country;

    @FXML
    private DatePicker birthDate;

    @FXML
    private RadioButton male, female;

    @FXML
    ToggleGroup sex;

    @FXML
    Button validate, bt_addTreeItem, bt_removeTreeItem;

    @FXML
    PieChart nbrContactPerGroup;

    @FXML
    BarChart <String, Number> histContactPerCity;

    @FXML
    BarChart <Number, String> histContactPerAge;



    private Workspace _workspace;
    private TreeItem<Object> _root;
    private Contact _editingContact;
    private HashMap<String, List<Control>> _validation;
    private Graphique _graphique;

    public ContactController(){
        _workspace = new Workspace();
        _groupIcon = new Image("ressources/images/group.png");
        _contactIcon = new Image("ressources/images/contact.png");
        _editingContact = new Contact();
        _validation = new HashMap();
        _graphique = new Graphique(_workspace);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        /*---------------------------------------------------*/
        /*                                                   */
        /*              Initialisation                       */
        /*             ==============                        */
        /*---------------------------------------------------*/

        // initialisation => treeView
        //===================================================

        _root = new TreeItem<Object>("Fiche de Contacts");
        _root.setExpanded(true);

        tree.setRoot(_root);
        tree.setEditable(true);
        tree.setCellFactory(param -> new TextFieldTreeCellImpl(tree));

        // initialisation => formulaire de contact
        //===================================================

        containerContact.visibleProperty().set(false);

        Address address = _editingContact.getAddressProperty().getValue();
        country.getItems().addAll(Country.getListCountry());

        /*---------------------------------------------------*/
        /*                                                   */
        /*              binding : TreeView                   */
        /*          ============================             */
        /*---------------------------------------------------*/

        // tree => listener
        //===================================================
        tree.getSelectionModel().selectedItemProperty().addListener(
                (v, oldSelectedItem, selectedItem) -> {
                    containerContact.setVisible(false);
                    _editingContact.reset();

                    if(selectedItem == null) return;
                    if (selectedItem.getValue() instanceof Contact) {
                        Contact c = (Contact) selectedItem.getValue();
                        System.out.println(c.displayContact());
                        c.copy(_editingContact);
                        containerContact.setVisible(true);
                    }
                });

        // liste contact => listener
        //===================================================
        ListChangeListener<Contact> contactListener = change -> {
            change.next();
            if (change.wasAdded()) {
                change.getAddedSubList().forEach(c -> {
                    if (c != null) {
                        TreeItem<Object> contactItem = new TreeItem(c, new ImageView(_contactIcon));
                        Group g = (Group) c.getParent();
                        groupToTreeItem(g).getChildren().add(contactItem);
                    }
                });
            } else if (change.wasRemoved()) {
                change.getRemoved().forEach(c -> {
                    Group g = (Group) c.getParent();
                    TreeItem<Object> groupItem = groupToTreeItem(g);
                    TreeItem<Object> contactItem = contactToTreeItem(groupItem, c);
                    groupItem.getChildren().removeAll(contactItem);
                });
            }
        };

        // group => listener
        //===================================================

        ListChangeListener<Group> groupListener = change -> {
            change.next();
            if (change.wasAdded()) {
                change.getAddedSubList().forEach(g -> {
                    TreeItem<Object> groupItem = new TreeItem(g, new ImageView(_groupIcon));
                    groupItem.setExpanded(true);
                    _root.getChildren().add(groupItem);
                    g.getListContactProperty().addListener(contactListener);
                });
            } else if (change.wasRemoved()) {
                change.getRemoved().forEach(g -> {
                            TreeItem<Object> groupeItem = groupToTreeItem(g);
                            _root.getChildren().removeAll(groupeItem);
                        }
                );
            }
        };

        _workspace.getListGroupProperty().addListener(groupListener);


        /*---------------------------------------------------*/
        /*                                                   */
        /*            binding :  Graphique                   */
        /*            ========================               */
        /*---------------------------------------------------*/

        ListChangeListener<PieChart.Data> p = change -> {
            change.next();
            nbrContactPerGroup.setData(_graphique.getPieChartDataProperty());
        };

        _graphique.getPieChartDataProperty().addListener(p);

        _graphique.getHistContactPerCityProperty().addListener(
                (v, oldValue, newValue) -> {
                    histContactPerCity.getData().clear();
                    histContactPerCity.getData().add( newValue);
                });


        _graphique.getHistContactPerAgeProperty().addListener(
                (v, oldValue, newValue) -> {
                    histContactPerAge.getData().clear();
                    histContactPerAge.getData().addAll(newValue);
                });

        /*---------------------------------------------------*/
        /*                                                   */
        /*            binding :  error message               */
        /*            ========================               */
        /*---------------------------------------------------*/

        Alert alert = new Alert(Alert.AlertType.ERROR);
        _workspace.getErrorMessageProperty().addListener(
                (v, oldErrorMessage, newErrorMessage) -> {
                    alert.setTitle(newErrorMessage.getTitle());
                    alert.setHeaderText( newErrorMessage.getMessage());
                    alert.showAndWait();
                }
        );

        /*---------------------------------------------------*/
        /*                                                   */
        /*               binding :  contact                  */
        /*               ====================                */
        /*---------------------------------------------------*/

        firstname.textProperty().bindBidirectional(_editingContact.getFirstNameProperty());
        lastname.textProperty().bindBidirectional(_editingContact.getLastNameProperty());
        street.textProperty().bindBidirectional(address.getStreetProperty());
        postcode.textProperty().bindBidirectional(address.getPostcodeProperty());
        city.textProperty().bindBidirectional(address.getCityProperty());
        birthDate.valueProperty().bindBidirectional(_editingContact.getBirthDateProperty());

        country.getSelectionModel().selectedItemProperty().addListener(
                (v, oldValue, newValue) -> {
                    address.getSelectedCountryProperty().setValue(newValue);
                }
        );


        _editingContact.getAddressProperty().get().getSelectedCountryProperty().addListener(
                (v, oldValue, newValue) -> {
                    if (newValue == null || newValue.getCode().equals(""))
                        country.getSelectionModel().clearSelection();
                    else
                        country.getSelectionModel().select(newValue);
                }
        );

        sex.selectedToggleProperty().addListener((v, old_toggle, new_toggle) -> {
            if (sex.getSelectedToggle() != null) {
                RadioButton radio = (RadioButton) sex.getSelectedToggle();
                switch (radio.getText()) {
                    case "F":
                        _editingContact.getSexProperty().setValue("F");
                        break;
                    case "H":
                        _editingContact.getSexProperty().setValue("H");
                        break;
                    default:
                        _editingContact.getSexProperty().setValue("");
                }
            }
        });

        _editingContact.getSexProperty().addListener((v, o, n) -> {
            if(n == null) return;

            switch (n) {
                case "F":
                    sex.selectToggle(female);
                    break;
                case "H":
                    sex.selectToggle(male);
                    break;
                default:
                    sex.selectToggle(null);
            }
        });

        MapChangeListener<String, String> v = change -> {
            if (change.wasAdded()) {
                _validation.get(change.getKey()).forEach(c ->
                {
                    c.setStyle("-fx-border-color: red;");
                    c.setTooltip(new Tooltip(change.getValueAdded()));
                });
            } else if (change.wasRemoved())
                _validation.get(change.getKey()).forEach(
                        c -> {
                            c.setStyle("-fx-border-color: none;");
                            c.setTooltip(null);
                        });
        };

        _editingContact.getMap().addListener(v);

        /*------------------------------------------------*/
        /*                                                */
        /*                  Event Handler:                */
        /*               ====================             */
        /*----------------------------------------------- */

        bt_addTreeItem.setOnAction(event -> {
            TreeItem<Object> currentItem = tree.getSelectionModel().getSelectedItem();
            if (currentItem == null || currentItem.getValue() == null)
                _workspace.getListGroupProperty().add(new Group());
            else if (currentItem.getValue() instanceof Group)
                containerContact.setVisible(true);
            else if (currentItem.getValue() instanceof Contact) return;
            else _workspace.getListGroupProperty().add(new Group());
        });

        bt_removeTreeItem.setOnAction(event -> {
            TreeItem<Object> currentItem = tree.getSelectionModel().getSelectedItem();
            containerContact.setVisible(false);
            if (currentItem == null || currentItem.getValue() == null) return;
            else if (currentItem.getValue() instanceof Group) {
                _workspace.getListGroupProperty().removeAll((Group)currentItem.getValue());
            }
            else if (currentItem.getValue() instanceof Contact) {
                Contact removeContact = (Contact) currentItem.getValue();
                ((Group)removeContact.getParent()).getListContactProperty().removeAll(removeContact);
            }
        });

        validate.setOnAction(event -> {
            TreeItem<Object> currentItem = tree.getSelectionModel().getSelectedItem();

            if (_editingContact.validate() && currentItem != null && currentItem.getValue() != null) {
                if (currentItem.getValue() instanceof Group) {
                    Contact newContact = new Contact();
                    _editingContact.copy(newContact);
                    Group grp = (Group) currentItem.getValue();
                    newContact.setParent(grp);
                    grp.getListContactProperty().add(newContact);
                    tree.getSelectionModel().select(contactToTreeItem(currentItem, newContact));
                } else if (currentItem.getValue() instanceof Contact) {
                    Contact contact =  (Contact)currentItem.getValue();
                    currentItem.setValue(null);
                    _editingContact.copy(contact);
                    currentItem.setValue(contact);
                }
            }
        });

        openFile.setOnAction(event -> {
            File file = openFile("OPEN");
            if (file != null) {
                _workspace.setListGroup(_workspace.getWorkspacePersistence().fromFile(file));
                _graphique.rehydrate();
            }
        });

        saveFile.setOnAction(event -> {
            if (!_workspace.getWorkspacePersistence().hasFile()) {
                File file = openFile("SAVE");
                if (file == null) { return; }
                else _workspace.getWorkspacePersistence().setFile(file);
            }
            _workspace.getWorkspacePersistence().save();
        });

        saveAs.setOnAction(event -> {
            File file = openFile("SAVE");
            if (file == null) { return; }
            _workspace.getWorkspacePersistence().save(file);
        });


        /*--------------------------------------------------*/
        /*                                                  */
        /*                validation : contact              */
        /*                ====================              */
        /*--------------------------------------------------*/

        mapValidation();
    }

    public void mapValidation()
    {
        _validation.put(_editingContact.getFirstNameProperty().getName(), new ArrayList<Control>(){{add(firstname);}});
        _validation.put(_editingContact.getLastNameProperty().getName(), new ArrayList<Control>(){{add(lastname);}});
        _validation.put(_editingContact.getAddressProperty().get().getCityProperty().getName(), new ArrayList<Control>(){{add(city);}});
        _validation.put(_editingContact.getAddressProperty().get().getPostcodeProperty().getName(), new ArrayList<Control>(){{add(postcode);}});
        _validation.put(_editingContact.getAddressProperty().get().getStreetProperty().getName(), new ArrayList<Control>(){{add(street);}});
        _validation.put(_editingContact.getAddressProperty().get().getSelectedCountryProperty().getName(), new ArrayList<Control>(){{add(country);}});
        _validation.put(_editingContact.getSexProperty().getName(), new ArrayList<Control>(){{add(female); add(male);}});
        _validation.put(Constant.Constant.BIRTH_DATE, new ArrayList<Control>(){{add(birthDate);}});
    }

    public TreeItem<Object> groupToTreeItem(Group g){
        return _root.getChildren()
                .stream()
                .filter(f ->  g.equals(f.getValue()))
                .findFirst()
                .get();
    }

    public TreeItem<Object> contactToTreeItem(TreeItem<Object> g, Contact c){
        return g.getChildren()
                .stream()
                .filter(f ->  c.equals(f.getValue()))
                .findFirst()
                .get();
    }

    public File openFile(String type){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("."));

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JSON", "*.json"),
                new FileChooser.ExtensionFilter("BIN", "*.bin")
        );

        File file = null;
        if(type.equals("SAVE")) file = fileChooser.showSaveDialog(null);
        else if(type.equals("OPEN")) file =  fileChooser.showOpenDialog(null);

        return file;
    }
}
