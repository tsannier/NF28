package logic;

import Constant.DefaultValue;
import Model.Contact;
import Model.ErrorMessage;
import Model.Group;
import Model.Workspace;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Persistance {

    Workspace _workspace;
    private ObjectProperty<ErrorMessage> _errorMessage;
    private File _file;

    public Persistance(Workspace workspace){
        _workspace = workspace;
        _errorMessage = new SimpleObjectProperty<>(DefaultValue.ERROR_MESSAGE);
        _file = null;
    }

    public ErrorMessage getErrorMessage(){ return _errorMessage.get(); }

    public  ObjectProperty<ErrorMessage> getErrorMessageProperty(){ return _errorMessage; }

    public void setErrorMessage(ErrorMessage errorMessage){ _errorMessage.setValue(errorMessage);}


    public  boolean hasFile(){ return (_file == null) ?  false : true; }

    public File getFile() { return _file; }

    public void setFile(File file) { _file = file; }

    public String getFileExtention(){
        if(_file == null) return  null;
        String fileName = _file.getName();
        return fileName.substring(fileName.indexOf(".") + 1, _file.getName().length());
    }



    public ArrayList<Group> fromFile(File file){
        setFile(file);
        return  fromFile();
    }

    public ArrayList<Group> fromFile() {
        if (!hasFile()) return null;

        try {
            ArrayList<Group> listGroup = null;
            switch (getFileExtention()) {
                case "json":
                    listGroup = fromFileJson();
                    break;
                case "bin":
                    listGroup = fromFileBinary();
                    break;
            }

            return listGroup;
        } catch (IOException | ClassNotFoundException e) {
            setErrorMessage(new ErrorMessage(
                    "Ouverture du fichier",
                    "Impossible d'ouvrir le fichier " + getFileExtention()
            ));
            return null;
        }
    }

    private ArrayList<Group> fromFileBinary() throws ClassNotFoundException, IOException {

        ArrayList<Group> listGroup = new ArrayList<>();
        if(!hasFile()) return null;
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(getFile()));
        Object[] grs = (Object[]) ois.readObject();
        for (int i = 0; i < grs.length; i++) {
            Group g = (Group) grs[i];
            Group group = new Group(g.getGroupName());
            listGroup.add(group);
            Object[] l = g.getListContact().toArray();

            for (int j = 0; j < l.length; j++) {
                Contact contact = (Contact) l[j];
                contact.setParent(group);
                group.getListContactProperty().add(contact);
            }
        }
        ois.close();
        return listGroup;

    }


    private ArrayList<Group> fromFileJson() throws IOException{
        if(!hasFile()) return null;
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(getFile(), new TypeReference<List<Group>>() {});
    }



    public Boolean save(File file){
        setFile(file);
        return save();

    }

    public Boolean save(){
        if(!hasFile()) return false;

        try {
            switch (getFileExtention()) {
                case "json":
                    saveAsJson();
                    break;
                case "bin":
                    saveAsBinary();
                    break;
            }
            return true;
        }catch (IOException e) {
            setErrorMessage(new ErrorMessage(
                    "Sauvegarde du fichier",
                    "Impossible de sauvegarder le fichier " + getFileExtention()
            ));
            return false;
        }
    }

    private void saveAsBinary() throws  IOException{
        if (!hasFile()) return;
        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(getFile()));
        stream.writeObject(_workspace.getListGroup().toArray());
        stream.close();
    }

    private void saveAsJson() throws IOException{
        if (!hasFile()) return;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(getFile(), _workspace.getListGroup());
    }
}
