package Constant;

public class Constant {

    public static String STREET = "STREET";
    public static String POSTCODE = "POSTCODE";
    public static String CITY = "CITY";
    public static String FIRSTNAME = "FIRSTNAME";
    public static String LASTNAME = "LASTNAME";
    public static String SEX = "SEX";
    public static String COUNTRY = "COUNTRY";
    public static String BIRTH_DATE = "BIRTH_DATE";
    public static String GROUP_NAME = "GROUP_NAME";

}
