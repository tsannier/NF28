package Model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Graphique {

    private Workspace _workspace;
    private ObservableList<PieChart.Data> _pieChartData;
    ObjectProperty<XYChart.Series<String, Number>> _histContactPerCity;
    ObjectProperty<ArrayList<XYChart.Series<Number, String>>> _histContactPerAge;

    public Graphique(Workspace workspace){
        _workspace = workspace;
        _pieChartData = FXCollections.observableArrayList();
        _histContactPerCity = new SimpleObjectProperty<>();
        _histContactPerAge = new SimpleObjectProperty<>();
    }

    public ObservableList<PieChart.Data> getPieChartDataProperty() {
        return _pieChartData;
    }

    public ObjectProperty<XYChart.Series<String, Number>> getHistContactPerCityProperty() {
        return _histContactPerCity;
    }

    public  ObjectProperty<ArrayList<XYChart.Series<Number, String>>> getHistContactPerAgeProperty() {
        return _histContactPerAge;
    }

    public void rehydrate(){


        /*---------------------------------------------------*/
        /*                                                   */
        /*                    Piechart                       */
        /*          ============================             */
        /*---------------------------------------------------*/

        List<PieChart.Data> data =
                _workspace.getListGroup()
                .stream()
                .map(group ->
                        new PieChart.Data(
                                group.getGroupName(),
                                group.getListContact().size()
                        )
                ).collect(Collectors.toList());

        _pieChartData.clear();
        _pieChartData.addAll(data);

        /*---------------------------------------------------*/
        /*                                                   */
        /*                    Piechart: city                 */
        /*          ============================             */
        /*---------------------------------------------------*/

        XYChart.Series<String, Number>  histContactPerCity = new XYChart.Series<>();

        _workspace.getListGroup()
                .stream()
                .flatMap(group -> group.getListContact().stream())
                .collect(
                        Collectors.groupingBy(
                                contact -> contact.getAddressProperty()
                                        .get()
                                        .getCityProperty()
                                        .getValue(),
                                Collectors.counting())
                )
                .forEach((city, number) -> histContactPerCity.getData().add(
                                new XYChart.Data<>(city, number)));

        getHistContactPerCityProperty().setValue(histContactPerCity);

        /*---------------------------------------------------*/
        /*                                                   */
        /*                    Piechart: age                  */
        /*          ============================             */
        /*---------------------------------------------------*/

        Predicate<Contact> pBefore = contact -> contact.getBirthDateProperty().get().toString().compareTo("1990-01-01") < 0;
        Predicate<Contact> pBetween = contact -> contact.getBirthDateProperty().get().toString().compareTo("1990-01-01") >= 0 &&  contact.getBirthDateProperty().get().toString().compareTo("2000-01-01") < 0;
        Predicate<Contact> pAfter = contact -> contact.getBirthDateProperty().get().toString().compareTo("2000-01-01") >= 0;

        ArrayList<XYChart.Series<Number, String>> series = new ArrayList<>();
        series.add(filterContactBirthdat(pBefore, "< 1990-01-01"));
        series.add(filterContactBirthdat(pBetween, "entre"));
        series.add( filterContactBirthdat(pAfter, ">= 2000-01-01"));

        _histContactPerAge.setValue(series);

    }

    private XYChart.Series<Number, String> filterContactBirthdat(Predicate<Contact> p, String name) {

        XYChart.Series<Number, String> serie = new XYChart.Series<>();
        serie.setName(name);

        _workspace.getListGroup()
                .stream()
                .flatMap(group -> group.getListContact().stream())
                .filter(p)
                .collect( Collectors.groupingBy(contact ->
                        ((Group)contact.getParent()).getGroupName(), Collectors.counting()))
                .forEach( (groupName, nb) -> {
                     serie.getData().add(new XYChart.Data<>(nb, groupName));
                });

        return serie;
    }
}








