package Constant;
import Model.Country;
import Model.ErrorMessage;

import java.time.LocalDate;

public class DefaultValue {

    public static final String FIRSTNAME = "";
    public static final String LASTANME = "";
    public static final String STREET = "";
    public static final String CITY = "";
    public static final String POSTCODE = "";
    public static final Country COUNTRY = null;
    public static final LocalDate BIRTHDATE = null;
    public static final String SEX =  "";
    public static final String GROUP_NAME = "Nom du groupe";
    public static final ErrorMessage ERROR_MESSAGE = null;
}