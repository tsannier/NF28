package App;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
    A faire
    1 - déplacer élément du workspace + création logic
    2 - sérialisation de workspace

    3 - vérification du type de fichier + appeller fonction sauvegarder en fonction

 */
public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("../View/contact.fxml"));
        primaryStage.setTitle("Formulaire de contact");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
