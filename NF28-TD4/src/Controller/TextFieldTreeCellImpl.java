package Controller;

import Model.Group;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.scene.input.KeyCode;

public final class TextFieldTreeCellImpl extends TreeCell<Object> {

    private TreeView<Object> _tree;
    private TextField textField;

    public TextFieldTreeCellImpl(TreeView<Object> tree) {
        this._tree = tree;
    }

    @Override
    public void startEdit() {

        if(!(_tree.getSelectionModel().getSelectedItem().getValue() instanceof Group))
            return;

        super.startEdit();

        if (textField == null) {
            createTextField();
        }
        setText(null);
        setGraphic(textField);
        textField.selectAll();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText((String) getItem());
        setGraphic(getTreeItem().getGraphic());
    }

    @Override
    public void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(getTreeItem().getGraphic());
            }
        }
    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setOnKeyReleased(t -> {
            if (t.getCode() == KeyCode.ENTER) {
                ((Group)getTreeItem().getValue()).getGroupNameProperty().setValue(textField.getText());
                commitEdit(getTreeItem().getValue());
            } else if (t.getCode() == KeyCode.ESCAPE) {
                cancelEdit();
            }
        });
    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
}