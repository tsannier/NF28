package Model;

public class ErrorMessage {

    String _title;
    String _message;


    public ErrorMessage(String title, String message){
        _title = title;
        _message = message;
    }

    public  String getTitle(){ return  _title; }

    public  void setTitle(String title){ _title = title; }

    public String getMessage(){ return _message;  }

    public void setMessage(String message){  _message = message;  }
}
