package Model;

import Constant.Constant;
import Constant.DefaultValue;
import Interface.Backtrack;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Contact implements Backtrack, Externalizable {

    private final StringProperty _firstName;
    private final StringProperty _lastName;
    private final ObjectProperty<Address> _address;
    private final ObjectProperty<LocalDate> _birthDate;
    private final StringProperty _sex;
    private ObservableMap<String, String> _map;
    private Object _parent;


    public Contact() {
        _firstName = new SimpleStringProperty(this, Constant.FIRSTNAME, DefaultValue.FIRSTNAME);
        _lastName = new SimpleStringProperty(this, Constant.LASTNAME, DefaultValue.LASTANME);
        _address = new SimpleObjectProperty<Address>(new Address());
        _birthDate = new SimpleObjectProperty<LocalDate>(DefaultValue.BIRTHDATE);
        _sex = new SimpleStringProperty(this, Constant.SEX, DefaultValue.SEX);
        _map = FXCollections.observableHashMap();
        _parent = null;
    }

    @JsonIgnore
    public StringProperty getFirstNameProperty() {
        return _firstName;
    }

    @JsonGetter("firstName")
    public String getFirstName() { return _firstName.get(); }

    @JsonSetter("firstName")
    public void setFirstName(String firstName) { _firstName.set(firstName); }

    @JsonIgnore
    public StringProperty getLastNameProperty() { return _lastName; }

    @JsonGetter("lastName")
    public String getLastName() { return _lastName.get(); }

    @JsonSetter("lastName")
    public void setLastName(String lastName) { _lastName.set(lastName); }

    @JsonIgnore
    public ObjectProperty<Address> getAddressProperty() {
        return _address;
    }

    @JsonGetter("address")
    public Address getAddress() { return _address.getValue();   }

    @JsonSetter("address")
    public void setAddress(Address address) { _address.set(address); }

    @JsonIgnore
    public ObjectProperty<LocalDate> getBirthDateProperty() {
        return _birthDate;
    }

    @JsonGetter("birthDate")
    public String getBirthDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        return _birthDate.get().format(formatter);
    }

    @JsonSetter("birthDate")
    public void setBirthDate(String birthDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate localDate = LocalDate.parse(birthDate, formatter);
        _birthDate.set(localDate);
    }

    @JsonIgnore
    public StringProperty getSexProperty() {
        return _sex;
    }

    @JsonGetter("sex")
    public String getSex() { return _sex.get();  }

    @JsonSetter("sex")
    public void setSex(String sex) { _sex.set(sex); }

    @JsonIgnore
    @Override
    public Object getParent() {
        return _parent;
    }

    @JsonIgnore
    public void setParent(Object parent) {
        _parent = parent;
    }

    @JsonIgnore
    public ObservableMap<String, String> getMap() {
        return _map;
    }

    public boolean validate() {
        _map.clear();

        /* validation => firstanme  */
        if (_firstName.getValue() == null || _firstName.getValue().trim().isEmpty())
            _map.put(_firstName.getName(), "prénom est un champ obligatoire");
        else if (_firstName.getValue().trim().length() < 2)
            _map.put(_firstName.getName(), "prénom doit contenir au moins 2 charactères");

        /* validation => lastname  */
        if (_lastName.getValue() == null || _lastName.getValue().trim().isEmpty())
            _map.put(_lastName.getName(), "nom est un champ obligatoire");
        else if (_lastName.getValue().trim().length() < 2)
            _map.put(_lastName.getName(), "nom doit contenir au moins 2 charactères");

        /* validation => city  */
        if (_address.getValue().getCityProperty().getValue() == null || _address.getValue().getCityProperty().getValue().trim().isEmpty())
            _map.put(_address.get().getCityProperty().getName(), "La ville est un champ obligatoire");

        /* validation => postcode  */
        if (_address.getValue().getPostcodeProperty().getValue() == null || _address.getValue().getPostcodeProperty().getValue().trim().isEmpty())
            _map.put(_address.get().getPostcodeProperty().getName(), "Le code postal est un champ obligatoire");

        /* validation => street  */
        if (_address.getValue().getStreetProperty().getValue() == null || _address.getValue().getStreetProperty().getValue().trim().isEmpty())
            _map.put(_address.get().getStreetProperty().getName(), "La  voie est un champ obligatoire");

        /* validation => country  */
        if (_address.get().getSelectedCountryProperty().getValue() == null || _address.get().getSelectedCountryProperty().getValue().getCode().equals("") || _address.get().getSelectedCountryProperty().getValue().getCode() == null)
            _map.put(_address.get().getSelectedCountryProperty().getName(), "Auncun pays sélectionné");

        if (_birthDate.getValue() == null)
            _map.put(_birthDate.getName(), "La  date de naissance est un champ obligatoire");

        /* validation => sex  */
        if (_sex.getValue() == null || _sex.getValue().equals("") || !(_sex.getValue().equals("F") || _sex.getValue().equals("H")))
            _map.put(_sex.getName(), "sex est un champ obligatoire");

        return _map.isEmpty();
    }

    public void reset() {
        this._firstName.setValue(DefaultValue.FIRSTNAME);
        this._lastName.setValue(DefaultValue.LASTANME);
        this._address.getValue().getSelectedCountryProperty().setValue(DefaultValue.COUNTRY);
        this._address.getValue().getStreetProperty().setValue(DefaultValue.STREET);
        this._address.getValue().getCityProperty().setValue(DefaultValue.CITY);
        this._address.getValue().getPostcodeProperty().setValue(DefaultValue.POSTCODE);
        this._birthDate.setValue(DefaultValue.BIRTHDATE);
        this._sex.setValue(DefaultValue.SEX);
        this._map.clear();
    }

    public void copy(Contact contact) {
        contact.getFirstNameProperty().setValue(this._firstName.getValue());
        contact.getLastNameProperty().setValue(this._lastName.getValue());
        this._address.getValue().copy(contact.getAddressProperty().getValue());
        contact.getBirthDateProperty().setValue(this._birthDate.getValue());
        contact.getSexProperty().setValue(this._sex.getValue());
        contact.setParent(this._parent);
        _map.clear();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(_firstName.get());
        out.writeUTF(_lastName.get());
        out.writeObject(_address.get());
        out.writeObject(_birthDate.get());
        out.writeUTF(_sex.get());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _firstName.set(in.readUTF());
        _lastName.set(in.readUTF());
        _address.setValue((Address) in.readObject());
        _birthDate.set((LocalDate) in.readObject());
        _sex.set(in.readUTF());
    }

    @Override
    public String toString() {
        return _firstName.getValue() + " " + _lastName.get();
    }

    public String displayContact() {
        return  "============================================== \n"
                + "firstname :" + _firstName.get() + "\n"
                + "lastname :" + _lastName.get() + "\n"
                + _address.get().displayAdress()
                + "birthdate :" + _birthDate.get() + "\n"
                + "sexe :" + _sex.get() + "\n"
                +"==============================================\n";
    }
}
