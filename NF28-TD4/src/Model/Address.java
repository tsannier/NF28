package Model;

import Constant.Constant;
import Constant.DefaultValue;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Address implements Cloneable, Externalizable {

    private final StringProperty _street;
    private final StringProperty _postcode;
    private final StringProperty _city;
    private final ObjectProperty<Country> _selectedCountry;

    public Address() {
        this._street = new SimpleStringProperty(this, Constant.STREET, DefaultValue.STREET);
        this._postcode = new SimpleStringProperty(this, Constant.POSTCODE, DefaultValue.POSTCODE);
        this._city = new SimpleStringProperty(this, Constant.CITY, DefaultValue.CITY);
        this._selectedCountry = new SimpleObjectProperty(DefaultValue.COUNTRY);
    }

    @JsonIgnore
    public StringProperty getStreetProperty() {
        return _street;
    }

    @JsonGetter("street")
    public String getStreet() { return _street.get(); }

    @JsonSetter("street")
    public void setStreet(String street) { _street.set(street); }

    @JsonIgnore
    public StringProperty getPostcodeProperty() {
        return _postcode;
    }

    @JsonGetter("postCode")
    public String getPostcode() { return _postcode.get(); }

    @JsonSetter("postCode")
    public void setPostcode(String postcode) { _postcode.set(postcode); }

    @JsonIgnore
    public StringProperty getCityProperty() {
        return _city;
    }

    @JsonGetter("city")
    public String getCity() { return _city.get(); }

    @JsonSetter("city")
    public void setCity(String city) { _city.set(city); }

    @JsonIgnore
    public ObjectProperty<Country> getSelectedCountryProperty() {
        return _selectedCountry;
    }

    @JsonGetter("countrySelected")
    public Country getSelectedCountry() { return _selectedCountry.get(); }

    @JsonSetter("countrySelected")
    public void setSelectedCountry(Country country) { _selectedCountry.set(country); }

    public void copy(Address adress) {
        adress.getStreetProperty().setValue(this._street.getValue());
        adress.getPostcodeProperty().setValue(this._postcode.getValue());
        adress.getCityProperty().setValue(this._city.getValue());
        adress.getSelectedCountryProperty().setValue(this._selectedCountry.getValue());
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(_street.get());
        out.writeUTF(_city.get());
        out.writeUTF(_postcode.get());
        out.writeObject(_selectedCountry.get());
    }


    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _street.set(in.readUTF());
        _city.set(in.readUTF());
        _postcode.set(in.readUTF());
        _selectedCountry.set((Country) in.readObject());
    }

    public String displayAdress() {
        return    "street :" + _street.get() + "\n"
                + "postcode :" + _postcode.get() + "\n"
                + "city :" + _city.get() + "\n"
                + "country :" + _selectedCountry.get().toString() + "\n";
    }
}


