package Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import logic.Persistance;

import java.util.List;
import java.util.stream.Collectors;


public class Workspace {

    private ObservableList<Group> _listGroup;

    public Workspace() {
        _listGroup = FXCollections.observableArrayList();
    }

    public ObservableList<Group> getListGroupProperty() {
        return _listGroup;
    }

    public List<Group> getListGroup(){ return _listGroup.stream().collect(Collectors.toList()); }


    public void setListGroup (List<Group> listGroup){
        _listGroup.clear();
        listGroup.forEach(group -> {
            List<Contact> listContact =  group.getListContact();
            group.setListContact(null);
            _listGroup.add(group);
            group.setListContact(listContact);
        });
    }


}
