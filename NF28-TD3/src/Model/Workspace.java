package Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;

public class Workspace {

    private ObservableList<Group> _listGroup;
    private File _file;

    public Workspace() {
        _listGroup = FXCollections.observableArrayList();
        _file = null;
    }

    public ObservableList<Group> getListGroup() {
        return _listGroup;
    }

    public File getFile() {
        return _file;
    }

    public void setFile(File file) {
        _file = file;
    }

    public void loadFile(File file) throws IOException, ClassNotFoundException {
            _listGroup.clear();
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            Object[] grs = (Object[]) ois.readObject();
            for (int i = 0; i < grs.length; i++) {
                Group g = (Group) grs[i];
                Group group = new Group(g.getGroupName().get());
                _listGroup.add(group);
                Object[] l = g.getListContact().toArray();

                for (int j = 0; j < l.length; j++) {
                    Contact contact = (Contact) l[j];
                    contact.setParent(group);
                    group.getListContact().add(contact);
                }
            }
            ois.close();
            setFile(file);
    }

    public void save() throws IOException{

        if (_file == null) return;
        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(getFile()));
        stream.writeObject(_listGroup.toArray());
        stream.close();
    }
}
