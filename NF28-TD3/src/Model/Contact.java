package Model;

import Constant.Constant;
import Constant.DefaultValue;
import Interface.Backtrack;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.LocalDate;


public class Contact implements Backtrack, Externalizable {

    private final StringProperty _firstname;

    ;
    private final StringProperty _lastname;
    private final ObjectProperty<Address> _address;
    private final ObjectProperty<LocalDate> _birthdate;
    private final StringProperty _sex;
    private ObservableMap<String, String> map;
    private Object _parent;
    public Contact(Object parent) {
        this();
        _parent = parent;
    }

    public Contact() {
        _firstname = new SimpleStringProperty(this, Constant.FIRSTNAME, DefaultValue.FIRSTNAME);
        _lastname = new SimpleStringProperty(this, Constant.LASTNAME, DefaultValue.LASTANME);
        _address = new SimpleObjectProperty<Address>(new Address());
        _birthdate = new SimpleObjectProperty<LocalDate>(DefaultValue.BIRTHDATE);
        _sex = new SimpleStringProperty(this, Constant.SEX, DefaultValue.SEX);
        map = FXCollections.observableHashMap();
        _parent = null;
    }

    public StringProperty getFirstnameProperty() {
        return _firstname;
    }

    public StringProperty getLastNameProperty() {
        return _lastname;
    }

    public ObjectProperty<Address> getAddressProperty() {
        return _address;
    }

    public ObjectProperty<LocalDate> getBirthdateProperty() {
        return _birthdate;
    }

    public StringProperty getSexProperty() {
        return _sex;
    }

    public ObservableMap<String, String> getMap() {
        return map;
    }

    @Override
    public Object getParent() {
        return _parent;
    }

    public void setParent(Object parent) {
        _parent = parent;
    }

    public boolean validate() {
        map.clear();

        /* validation => firstanme  */
        if (_firstname.getValue() == null || _firstname.getValue().trim().isEmpty())
            map.put(_firstname.getName(), "prénom est un champ obligatoire");
        else if (_firstname.getValue().trim().length() < 2)
            map.put(_firstname.getName(), "prénom doit contenir au moins 2 charactères");

        /* validation => lastname  */
        if (_lastname.getValue() == null || _lastname.getValue().trim().isEmpty())
            map.put(_lastname.getName(), "nom est un champ obligatoire");
        else if (_lastname.getValue().trim().length() < 2)
            map.put(_lastname.getName(), "nom doit contenir au moins 2 charactères");

        /* validation => city  */
        if (_address.getValue().getCityProperty().getValue() == null || _address.getValue().getCityProperty().getValue().trim().isEmpty())
            map.put(_address.get().getCityProperty().getName(), "La ville est un champ obligatoire");

        /* validation => postcode  */
        if (_address.getValue().getPostcodeProperty().getValue() == null || _address.getValue().getPostcodeProperty().getValue().trim().isEmpty())
            map.put(_address.get().getPostcodeProperty().getName(), "Le code postal est un champ obligatoire");

        /* validation => street  */
        if (_address.getValue().getStreetProperty().getValue() == null || _address.getValue().getStreetProperty().getValue().trim().isEmpty())
            map.put(_address.get().getStreetProperty().getName(), "La  voie est un champ obligatoire");

        /* validation => country  */
        if (_address.get().getselectedContryProperty().getValue() == null || _address.get().getselectedContryProperty().getValue().getCode().equals("") || _address.get().getselectedContryProperty().getValue().getCode() == null)
            map.put(_address.get().getselectedContryProperty().getName(), "Auncun pays sélectionné");

        if (_birthdate.getValue() == null)
            map.put(_birthdate.getName(), "La  date de naissance est un champ obligatoire");


        /* validation => sex  */
        if (_sex.getValue() == null || _sex.getValue().equals("") || !(_sex.getValue().equals("F") || _sex.getValue().equals("H")))
            map.put(_sex.getName(), "sex est un champ obligatoire");

        return map.isEmpty();
    }

    public void reset() {
        this._firstname.setValue(DefaultValue.FIRSTNAME);
        this._lastname.setValue(DefaultValue.LASTANME);
        this._address.getValue().getselectedContryProperty().setValue(DefaultValue.COUNTRY);
        this._address.getValue().getStreetProperty().setValue(DefaultValue.STREET);
        this._address.getValue().getCityProperty().setValue(DefaultValue.CITY);
        this._address.getValue().getPostcodeProperty().setValue(DefaultValue.POSCODE);
        this._birthdate.setValue(DefaultValue.BIRTHDATE);
        this._sex.setValue(DefaultValue.SEX);
        this.map.clear();
    }

    public void copy(Contact contact) {
        contact.getFirstnameProperty().setValue(this._firstname.getValue());
        contact.getLastNameProperty().setValue(this._lastname.getValue());
        this._address.getValue().copy(contact.getAddressProperty().getValue());
        contact.getBirthdateProperty().setValue(this._birthdate.getValue());
        contact.getSexProperty().setValue(this._sex.getValue());
        contact.setParent(this._parent);
        map.clear();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(_firstname.get());
        out.writeUTF(_lastname.get());
        out.writeObject(_address.get());
        out.writeObject(_birthdate.get());
        out.writeUTF(_sex.get());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _firstname.set(in.readUTF());
        _lastname.set(in.readUTF());
        _address.setValue((Address) in.readObject());
        _birthdate.set((LocalDate) in.readObject());
        _sex.set(in.readUTF());
    }

    @Override
    public String toString() {
        return _firstname.getValue() + " " + _lastname.get();
    }

    public String displayContact() {
        return  "============================================== \n"
                + "firstname :" + _firstname.get() + "\n"
                + "lastname :" + _lastname.get() + "\n"
                + _address.get().displayAdress()
                + "birthdate :" + _birthdate.get() + "\n"
                + "sexe :" + _sex.get() + "\n"
                +"==============================================\n";
    }
}
