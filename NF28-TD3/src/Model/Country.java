package Model;

import Constant.Constant;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.*;

public class Country implements Cloneable, Externalizable {

    private static ObservableList<Country> _listCountry = FXCollections.observableArrayList();
    private final StringProperty _code;
    private String _name;

    public Country(String code) {
        this();
        this.setCode(code);
    }

    public Country() {
        _code = new SimpleStringProperty(this, Constant.COUNTRY, "");
        _name = "";
    }

    public static ObservableList<Country> getListCountry() {
        if (Country._listCountry.isEmpty()) {
            String[] countryCodes = Locale.getISOCountries();
            for (String countryCode : countryCodes) {
                Country._listCountry.add(new Country(countryCode));
            }
        }
        return Country._listCountry.sorted();
    }

    public String getCode() {
        return _code.getValue();
    }

    public void setCode(String code) {

        if (code != null && code != "") {
            Locale locale = new Locale("", code);
            Set<String> listCode = new HashSet<String>(Arrays.asList(Locale.getISOCountries()));
            if (listCode.contains(code)) {
                _code.setValue(locale.getCountry());
                _name = locale.getDisplayName();
                return;
            }
        }
        _code.setValue("");
        _name = "";
    }

    public String getCodePropertyName() {
        return _code.getName();
    }

    @Override
    public Country clone() {
        Country country = new Country(this._code.getValue());
        return country;
    }

    @Override
    public String toString() {
        return _name;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(_name);
        out.writeUTF(_code.get());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _name = in.readUTF();
        _code.set(in.readUTF());
    }
}