package Model;

import Constant.Constant;
import Constant.DefaultValue;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Address implements Cloneable, Externalizable {

    private final StringProperty _street;
    private final StringProperty _postcode;
    private final StringProperty _city;
    private final ObjectProperty<Country> _selectedCountry;

    public Address() {
        this._street = new SimpleStringProperty(this, Constant.STREET, DefaultValue.STREET);
        this._postcode = new SimpleStringProperty(this, Constant.POSTCODE, DefaultValue.POSCODE);
        this._city = new SimpleStringProperty(this, Constant.CITY, DefaultValue.CITY);
        this._selectedCountry = new SimpleObjectProperty(DefaultValue.COUNTRY);
    }

    public StringProperty getStreetProperty() {
        return _street;
    }

    public StringProperty getPostcodeProperty() {
        return _postcode;
    }

    public StringProperty getCityProperty() {
        return _city;
    }

    public ObjectProperty<Country> getselectedContryProperty() {
        return _selectedCountry;
    }

    public void copy(Address adress) {

        adress.getStreetProperty().setValue(this._street.getValue());
        adress.getPostcodeProperty().setValue(this._postcode.getValue());
        adress.getCityProperty().setValue(this._city.getValue());
        adress.getselectedContryProperty().setValue(this._selectedCountry.getValue());
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(_street.get());
        out.writeUTF(_city.get());
        out.writeUTF(_postcode.get());
        out.writeObject(_selectedCountry.get());
    }


    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _street.set(in.readUTF());
        _city.set(in.readUTF());
        _postcode.set(in.readUTF());
        _selectedCountry.set((Country) in.readObject());
    }

    public String displayAdress() {
        return    "street :" + _street.get() + "\n"
                + "postcode :" + _postcode.get() + "\n"
                + "city :" + _city.get() + "\n"
                + "country :" + _selectedCountry.get().toString() + "\n";
    }
}


