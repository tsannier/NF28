package Model;

import Constant.Constant;
import Constant.DefaultValue;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.LinkedHashSet;
import java.util.Set;

public class Group implements Externalizable {

    private final ObservableList<Contact> _listContact;
    private final StringProperty _groupName;

    public Group(String groupName) {
        this();
        _groupName.setValue(groupName);
    }

    public Group() {
        _listContact = FXCollections.observableArrayList();
        _groupName = new SimpleStringProperty(this, Constant.GROUP_NAME, DefaultValue.GROUP_NAME);
    }

    public ObservableList<Contact> getListContact() {
        return _listContact;
    }

    public StringProperty getGroupName() {
        return _groupName;
    }

    @Override
    public String toString() {
        return _groupName.getValue();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(_groupName.get());
        out.writeObject(new LinkedHashSet<>(_listContact));
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException,
            ClassNotFoundException {
        _groupName.set(in.readUTF());
        _listContact.clear();
        ((Set<Contact>) in.readObject()).forEach(c -> {
            _listContact.add(c);
            c.setParent(this);
        });
    }


}
