package td01.controller;

import javafx.scene.image.Image;
import javafx.util.converter.NumberStringConverter;
import td01.model.Model;
import td01.view.RootViewElement;
import td01.constant.*;
import java.io.File;
import java.net.MalformedURLException;

public class Controller {

	protected Model model;
	protected RootViewElement view;


	public Controller(RootViewElement view) {
		this.model = new Model();
		this.view = view;
		initialize();
	}

	public void initialize() {

		// binding
		//----------------------------------
		this.model.getImageNameProperty().addListener( (obs, oldv, newv) ->{
			try {
				String path = new File("ressources/" + model.getImageName()).toURI().toURL().toString();
				this.view.getImageView().setImage(new Image(path, DefaultValue.IMAGE_WIDTH , DefaultValue.IMAGE_HEIGHT , false, false));
			}
			catch (  MalformedURLException e){
				e.printStackTrace();
			}
		});

		view.getBtnStart().setOnAction(evt -> {
			model.start();
			view.getBtnStop().setDisable(false);
			view.getBtnStart().setDisable(true);
		});

		view.getBtnStop().setOnAction(evt -> {
			model.stop();
			view.getBtnStop().setDisable(true);
			view.getBtnStart().setDisable(false);
		});

		view.getTextField().textProperty().bindBidirectional(model.getIntervalProperty(), new NumberStringConverter());



		model.getIntervalProperty().addListener(
				(v,oldValue,newValue) -> {
					view.getSlider().valueProperty().setValue(newValue.intValue()/1000);

					if(model.getIsStart()) {
						model.stop();
						model.start();
					}
				}
		);

		view.getSlider().valueProperty().addListener(
				(v,oldValue, newValue) -> {
					model.getIntervalProperty().setValue(newValue.intValue() * 1000);
				}
		);


		// Initialisation
		//----------------------------------

		try {
			view.getBtnStop().setDisable(true);
			String path = new File("ressources/" + model.getImageName()).toURI().toURL().toString();
			this.view.getImageView().setImage(new Image(path, DefaultValue.IMAGE_WIDTH , DefaultValue.IMAGE_HEIGHT , false, false));
			model.getIntervalProperty().setValue(1000);
		}
		catch (  MalformedURLException e){
			e.printStackTrace();
		}

	}


	
}
