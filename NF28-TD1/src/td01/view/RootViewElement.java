package td01.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import td01.controller.NumberTextField;

public class RootViewElement extends HBox {

	protected ImageView _imageView;
	protected TextField _tf_time;
	protected Button _bt_start;
	protected Button _bt_stop;
	protected Slider _slider;

	public RootViewElement() {

		this.prefHeight(300);
		this.prefWidth(600);
		this.setPadding(new Insets(20, 20, 20, 20));


		// Settings
		//--------------------------------------------------------------------
		VBox vb_setting = new VBox();
		vb_setting.prefWidth(300);
		vb_setting.prefHeight(280);
		vb_setting.setPadding(new Insets(10));
		vb_setting.setSpacing(35);

		// textfield => Time
		HBox hb_time = new HBox();
		hb_time.setSpacing(20);

		Label lb_time = new Label("Intervalle (millis) : ");
		_tf_time = new NumberTextField();

		hb_time.getChildren().addAll(lb_time, _tf_time);

		// button => start & stop
		HBox hb_button = new HBox();
		hb_button.setSpacing(20);
		hb_button.setAlignment(Pos.CENTER);

		_bt_start = new Button("Start");
		_bt_start.setPadding(new Insets(5,25,5,25));
		_bt_stop = new Button("Stop");
		_bt_stop.setPadding(new Insets(5,25,5,25));


		hb_button.getChildren().addAll(_bt_start, _bt_stop);

		// slider
		_slider = new Slider();
		_slider.setMax(10);
		_slider.setShowTickLabels(true);
		_slider.setShowTickMarks(true);
		_slider.setMajorTickUnit(1);
		_slider.setMinorTickCount(1);

		vb_setting.getChildren().addAll(hb_time,hb_button, _slider);

		// Image
		//--------------------------------------------------------------------
		_imageView = new ImageView();
		_imageView.setFitHeight(240);
		_imageView.setFitWidth(220);

		this.getChildren().addAll(vb_setting, _imageView);
	}

	public ImageView getImageView() {
		return _imageView;
	}

	public TextField getTextField() {
		return _tf_time;
	}

	public Button getBtnStart() {
		return _bt_start;
	}

	public Button getBtnStop() {
		return _bt_stop;
	}

	public Slider getSlider() {
		return _slider;
	}
}
