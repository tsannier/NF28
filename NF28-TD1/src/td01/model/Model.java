package td01.model;

import java.io.File;
import java.util.*;

import javafx.beans.property.*;
import td01.constant.*;

public class Model {

	final String IMAGE_DIR = "ressources/";

	private IntegerProperty _interval;
	private StringProperty _imageName;
	private List<String> _imageList;
	private Timer _timer;

	public Model(){

	initialisation();
	}

	public  void initialisation(){

		_imageList = Arrays.asList(new File(IMAGE_DIR).list());
		_imageList.sort(String.CASE_INSENSITIVE_ORDER);

		_interval = new SimpleIntegerProperty(this, Constant.INTERVAL, DefaultValue.INTERVAL_DF);

		_imageName = new SimpleStringProperty(this, Constant.IMAGENAME, _imageList.get(0));
	}

	public void nextImage(){

		int index = _imageList.indexOf(_imageName.getValue());
		if(index < _imageList.size() - 1)
			_imageName.setValue(_imageList.get(index + 1));
		else
			_imageName.setValue(_imageList.get(0));
	}

	public int getInterval(){
		return _interval.get();
	}

	public IntegerProperty getIntervalProperty(){
		return _interval;
	}

	public  String getImageName(){
		return _imageName.get();
	}

	public  StringProperty getImageNameProperty(){
		return _imageName;
	}

	public boolean getIsStart(){
		return (_timer == null)? false : true;
	}

	public void start(){
		_timer = new Timer();
		if(_interval.get() > 0)
			_timer.schedule(new ImageTimerTask(this), 0, _interval.get());
	}

	public void stop(){
		if(_timer != null)
			_timer.cancel();
		_timer = null;
	}

	public IntegerProperty IntervalProperty(){
		return _interval;
	}

	public StringProperty imageNameProperty(){
		return  _imageName;
	}
}
