package td01.model;

import java.util.TimerTask;

public class ImageTimerTask extends TimerTask {
    Model _model;

    public ImageTimerTask(Model model){
        _model = model;
    }

    @Override
    public void run() {
        _model.nextImage();
        System.out.println(_model.imageNameProperty().getValue());
    }
}
