package td01;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import td01.controller.Controller;
import td01.view.RootViewElement;

public class TDApplication extends Application {
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Affichage d'images");
		RootViewElement root = new RootViewElement();
		new Controller(root);
		stage.setScene(new Scene(root, 600, 300));
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}

