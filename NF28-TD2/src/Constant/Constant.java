package Constant;


public class Constant {

    public static String STREET = "STREET";
    public static String POSTCODE = "POSTCODE";
    public static String CITY = "CITY";
    public static String FIRSTNAME = "FIRSTNAME";
    public static String LASTNAME = "LASTNAME";
    public static String SEX = "SEX";
    public static String BIRTHDATE = "BIRTHDATE";
    public static String  SELECTED_COUNTRY = "public static String";
    public static String COUNTRYCODE = "COUNTRYCODE";
    public static String COUNTRYNAME = "COUNTRYNAME";

}
