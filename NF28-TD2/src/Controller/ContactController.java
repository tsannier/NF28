package Controller;

import Model.Address;
import Model.Contact;
import Model.Country;
import javafx.collections.MapChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

public class ContactController implements Initializable {

    @FXML
    private TextField firstname;

    @FXML
    private TextField lastname;

    @FXML
    private TextField street;

    @FXML
    private TextField postcode;

    @FXML
    private TextField city;

    @FXML
    private ComboBox<Country> country;

    @FXML
    private DatePicker birthDate;

    @FXML
    private RadioButton male, female;

    @FXML
    ToggleGroup sex;

    @FXML
    Button validate;

    private HashMap<String,  List<Control>> validation;


    private Contact contact;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        contact = new Contact();
        Address address = contact.getAddressProperty().get();

        /*--------------------------------------------------*/
        /*                                                  */
        /*                  Initialisation                  */
        /*                  ==============                  */
        /*--------------------------------------------------*/

         // initialisation => country
        // ===================================================

        country.getItems().addAll(address.getListCountry());

        Country selectedCountryProperty = contact.getAddressProperty().get().getselectedContryProperty().getValue();
        if(selectedCountryProperty != null && selectedCountryProperty.getCode() != "")
            country.getSelectionModel().select(selectedCountryProperty);


        // initialisation => birthdate
        // ===================================================

        if(contact.getBirthdateProperty().getValue() != null)
            birthDate.setValue(contact.getBirthdateProperty().getValue());

        // initialisation => sexe
        // ===================================================

        if(contact.getSexProperty().getValue() != null && contact.getSexProperty().getValue() == "F")
            sex.selectToggle(female);
        else  if(contact.getSexProperty().getValue() != null && contact.getSexProperty().getValue() == "H")
            sex.selectToggle(male);

        /*-------------------------------------------------*/
        /*                                                  */
        /*               binding :  contact                 */
        /*               ====================               */
        /*--------------------------------------------------*/


        firstname.textProperty().bindBidirectional(contact.getFirstnameProperty());
        lastname.textProperty().bindBidirectional(contact.getLastNameProperty());
        street.textProperty().bindBidirectional(address.getStreetProperty()); ;
        postcode.textProperty().bindBidirectional(address.getPostcodeProperty());
        city.textProperty().bindBidirectional(address.getCityProperty());
        birthDate.valueProperty().bindBidirectional(contact.getBirthdateProperty());


        country.getSelectionModel().selectedItemProperty().addListener(
                (v, oldValue, newValue) -> {
                    address.getselectedContryProperty().setValue(newValue);
                }
        ) ;

        contact.getAddressProperty().get().getselectedContryProperty().addListener(
                (v, oldValue, newValue) -> {
                    if(newValue == null || newValue.getCode() ==  "")
                        country.getSelectionModel().select(null);
                    else
                        country.getSelectionModel().select(newValue);
                }
        );


        sex.selectedToggleProperty().addListener((v, old_toggle, new_toggle) -> {
            if (sex.getSelectedToggle() != null) {
                RadioButton radio = (RadioButton) sex.getSelectedToggle();
                switch (radio.getText()){
                    case "F" :  contact.getSexProperty().setValue("F"); break;
                    case "H" :  contact.getSexProperty().setValue("H"); break;
                    default:  contact.getSexProperty().setValue("");
                }
            }
        });

        contact.getSexProperty().addListener((v, o, n) -> {
            switch (n){
                case "F" : sex.selectToggle(female);  break;
                case "H" : sex.selectToggle(male);  break;
                default: sex.selectToggle(null);
            }
        });


        /*--------------------------------------------------*/
        /*                                                  */
        /*               validation : contact               */
        /*               ====================               */
        /*--------------------------------------------------*/

        mapValidation();

        validate.setOnAction(event -> {
            if(contact.validate()){
                System.out.println(contact);
            }

        });

        MapChangeListener<String, String> v = change -> {
            if (change.wasAdded()) {
                validation.get(change.getKey()).forEach(c ->
                {
                    c.setStyle("-fx-border-color: red;");
                    c.setTooltip(new Tooltip(change.getValueAdded()));
                });
            }
            else if (change.wasRemoved())
                validation.get(change.getKey()).forEach(
                        c -> {
                            c.setStyle("-fx-border-color: green;");
                            c.setTooltip(null);
                        });
        };

        contact.getMap().addListener(v);
    }

    public void mapValidation()
    {
        validation = new HashMap<String, List<Control>>();
        validation.put(contact.getFirstnameProperty().getName(), new ArrayList<Control>(){{add(firstname);}});
        validation.put(contact.getLastNameProperty().getName(), new ArrayList<Control>(){{add(lastname);}});
        validation.put(contact.getAddressProperty().get().getCityProperty().getName(), new ArrayList<Control>(){{add(city);}});
        validation.put(contact.getAddressProperty().get().getPostcodeProperty().getName(), new ArrayList<Control>(){{add(postcode);}});
        validation.put(contact.getAddressProperty().get().getStreetProperty().getName(), new ArrayList<Control>(){{add(street);}});
        validation.put(contact.getAddressProperty().get().getselectedContryProperty().getValue().getCodePropertyName(), new ArrayList<Control>(){{add(country);}});
        validation.put(contact.getSexProperty().getName(), new ArrayList<Control>(){{add(female); add(male);}});
        validation.put(contact.getBirthdateProperty().getName(), new ArrayList<Control>(){{add(birthDate);}});
    }
}
