package Model;

import Constant.Constant;

import Constant.DefaultValue;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;


public class Country {

    private final StringProperty _code;
    private  String _name;

    public Country(String code) {
        this();
        this.setCode(code);
    }

    public Country(){
        _code = new SimpleStringProperty(this, Constant.COUNTRYCODE, "");
       _name = "";
    }

    public String getCode() {
        return _code.getValue();
    }

    public  String getCodePropertyName(){
        return _code.getName();
    }

    public void setCode(String code) {

        if(code != null && code != ""){

            Locale locale = new Locale("", code);

            Set<String> listCode = new HashSet<String>(Arrays.asList(Locale.getISOCountries()));

            if(listCode.contains(code)){
                _code.setValue(locale.getCountry());
                _name = locale.getDisplayName();
            }
        }
    }

    public String getName() {
        return _name;
    }

    @Override
    public String toString() {
        return _name;
    }
}

