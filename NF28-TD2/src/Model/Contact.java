package Model;

import Constant.Constant;
import Constant.DefaultValue;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.time.LocalDate;



public class Contact {

    public static  enum SEX {FEMALE,MALE, UNKNOWN};

    private final StringProperty _firstname;
    private final StringProperty _lastname;
    private final ObjectProperty<Address> _address;
    private final ObjectProperty<LocalDate> _birthdate;
    private final StringProperty _sex;
    private ObservableMap<String, String> map;

    public Contact(){
        _firstname = new SimpleStringProperty(this, Constant.FIRSTNAME, DefaultValue.FIRSTNAME);
        _lastname = new SimpleStringProperty(this, Constant.LASTNAME, DefaultValue.LASTANME);
        _address = new SimpleObjectProperty<Address>(new Address());
        _birthdate = new SimpleObjectProperty<LocalDate>(DefaultValue.BIRTHDATE);
        _sex = new SimpleStringProperty(this, Constant.SEX, DefaultValue.SEX);
        map = FXCollections.observableHashMap();
    }

    public StringProperty getFirstnameProperty() {
        return _firstname;
    }

    public StringProperty getLastNameProperty() {
        return _lastname;
    }

    public  ObjectProperty<Address> getAddressProperty(){
        return _address;
    }

    public ObjectProperty<LocalDate> getBirthdateProperty() {
        return _birthdate;
    }

    public StringProperty getSexProperty() {
        return _sex;
    }

    public ObservableMap<String, String> getMap() {
        return map;
    }

    public void setMap(ObservableMap<String, String> map) {
        this.map = map;
    }

    public boolean validate()
    {
        map.clear();

        /* validation => firstanme  */
        if(_firstname.getValue() == null || _firstname.getValue().trim().isEmpty()) map.put(_firstname.getName(), "prénom est un champ obligatoire");
        else if(_firstname.getValue().trim().length() < 2 ) map.put(_firstname.getName(), "prénom doit contenir au moins 2 charactères");

        /* validation => lastname  */
        if(_lastname.getValue() == null || _lastname.getValue().trim().isEmpty()) map.put(_lastname.getName(), "nom est un champ obligatoire");
        else if(_lastname.getValue().trim().length() < 2 ) map.put(_lastname.getName(), "nom doit contenir au moins 2 charactères");

        /* validation => city  */
        if(_address.getValue().getCityProperty().getValue() == null ||  _address.getValue().getCityProperty().getValue().trim().isEmpty()) map.put(_address.get().getCityProperty().getName(), "La ville est un champ obligatoire");

        /* validation => postcode  */
        if(_address.getValue().getPostcodeProperty().getValue() == null ||  _address.getValue().getPostcodeProperty().getValue().trim().isEmpty()) map.put(_address.get().getPostcodeProperty().getName(), "Le code postal est un champ obligatoire");

        /* validation => street  */
        if(_address.getValue().getStreetProperty().getValue() == null || _address.getValue().getStreetProperty().getValue().trim().isEmpty()) map.put(_address.get().getStreetProperty().getName(), "La  voie est un champ obligatoire");

        /* validation => country  */
        if(_address.get().getselectedContryProperty().getValue() == null || _address.get().getselectedContryProperty().getValue().getCode() == "" ||  _address.get().getselectedContryProperty().getValue().getCode() == null)
            map.put(_address.get().getselectedContryProperty().getValue().getCodePropertyName(), "Auncun pays sélectionné");

        if(_birthdate.getValue() == null) map.put(_birthdate.getName(), "La  date de naissance est un champ obligatoire");

        /* validation => sex  */
        if(_sex.getValue() == null || _sex.getValue() == "" || !(_sex.getValue() == "F" || _sex.getValue() == "H")) map.put(_sex.getName(), "sex est un champ obligatoire");

        return map.isEmpty();
    }

    @Override
    public String toString()
    {
        return "\n |-----------------------------------------------------------------------"
                +"\n | Prénom: " + _firstname.getValue()
                +"\n | Nom : " + _lastname.getValue()
                +"\n | Rue : " + _address.get().getStreetProperty().getValue()
                +"\n | Code postal : " + _address.get().getPostcodeProperty().getValue()
                +"\n | Ville : " + _address.get().getCityProperty().getValue()
                +"\n | Pays : " + _address.get().getselectedContry().toString()
                +"\n | Date de naissance : " + _birthdate.getValue()
                +"\n | Sexe : " + _sex.getValue()
                + "\n |-----------------------------------------------------------------------";
    }
}
