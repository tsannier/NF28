package Model;

import Constant.Constant;
import Constant.DefaultValue;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Locale;

public class Address {

    private final StringProperty _street;
    private final StringProperty _postcode;
    private final StringProperty _city;
    private final ObservableList<Country> _listCountry;
    private final ObjectProperty<Country> _selectedContry;

    public Address() {
        this._street = new SimpleStringProperty(this, Constant.STREET, DefaultValue.STREET);
        this._postcode = new SimpleStringProperty(this, Constant.POSTCODE, DefaultValue.POSCODE);
        this._city = new SimpleStringProperty(this, Constant.CITY, DefaultValue.CITY);
        this._listCountry =  FXCollections.observableArrayList();
        this._selectedContry = new  SimpleObjectProperty<Country>(new Country(DefaultValue.COUNTRY_CODE));
    }

    public StringProperty getStreetProperty() {
        return _street;
    }

    public StringProperty getPostcodeProperty() {
        return _postcode;
    }

    public StringProperty getCityProperty() {
        return _city;
    }

    public ObservableList<Country> getListCountry() {
        if(_listCountry.isEmpty()) {
            String[] countryCodes = Locale.getISOCountries();
            for (String countryCode : countryCodes) {
                Locale locale = new Locale("", countryCode);
                String code = locale.getCountry();
                _listCountry.add(new Country(code));
            }
        }
        return _listCountry;
    }

    public ObjectProperty<Country> getselectedContryProperty() {
        return _selectedContry;
    }

    public Country getselectedContry() {
        return _selectedContry.getValue();
    }

}


